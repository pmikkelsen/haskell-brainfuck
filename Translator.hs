{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Translator
  ( Translator
  , Optimizer
  , OptimizerState (..)
  , runOptimizer
  , optRest
  , opt
  , optMany
  , optManyWhen
  , optZero
  , optWhen
  , tryOpt
  , tryOptFailed
  , emit
  , emitMany
  , getState
  , getNodes
  , runTranslator
  , execTranslator
  , updateState
  , translate
  , fixedPoint
  ) where

import Control.Monad.RWS
import Control.Monad

newtype Translator from to state r =
  Translator {runTr :: RWS from to state r}
  deriving ( Functor
           , Applicative
           , Monad
           , MonadReader from
           , MonadWriter to
           , MonadState state)

data OptimizerState = OptState { taken :: Int
                               , produced :: Int
                               , round :: Int
                               }

type Optimizer a r = Translator a a OptimizerState r

runOptimizer :: (Eq a, Monoid a) => Optimizer a r -> a -> a
runOptimizer o = translate (fixedPoint o) (OptState 0 0 0)

finishOpt :: Monoid a => Int -> Optimizer a () -> Optimizer a ()
finishOpt produced o = do
  OptState taken p r <- getState
  when (p == 0) $ do
    o
    setState (OptState taken produced r)

tryOpt :: Int -> Optimizer [a] [a]
tryOpt n = do
  nodes <- getNodes
  OptState t p _ <- getState
  if p == 0
    then do updateState (\(OptState taken produced round) -> OptState n produced round)
            return $ take n nodes
    else return []

tryOptFailed :: Monoid a => Optimizer a ()
tryOptFailed = do
  OptState t p r <- getState
  when (p == 0) $ setState (OptState 0 0 r)

optRest :: Optimizer [a] () -> Optimizer [a] ()
optRest o = do
  nodes <- getNodes
  OptState t _ r <- getState
  unless (null nodes) $ do
    setState (OptState 0 0 r)
    runTranslator o (drop t nodes)
  when (null nodes) $ setState (OptState 0 0 (r+1))

opt :: (Applicative f, Monoid (f a)) => a -> Optimizer (f a) ()
opt x = finishOpt 1 $ emit x

optMany :: [a] -> Optimizer [a] ()
optMany x = finishOpt (length x) $ emitMany x

optZero :: Optimizer [a] ()
optZero = optMany []

optWhen :: (Applicative f, Monoid (f a)) => Bool -> a -> Optimizer (f a) ()
optWhen pred x = when pred (opt x)

optManyWhen :: Bool -> [a] -> Optimizer [a] ()
optManyWhen pred x = when pred (optMany x)

emitMany :: Monoid b => b -> Translator a b s ()
emitMany = tell

emit :: (Applicative f, Monoid (f b)) => b -> Translator a (f b) s ()
emit = tell . pure

emitWhen :: (Applicative f, Monoid (f b)) => Bool -> b -> Translator a (f b) s ()
emitWhen pred code = when pred (emit code)

getState :: Monoid b => Translator a b s s
getState = get

getNodes :: Monoid b => Translator a b s a
getNodes = ask

runTranslator :: Monoid b => Translator a b s r -> a -> Translator a b s r
runTranslator c x = local (const x) c

execTranslator :: Monoid b => Translator a b s r -> s -> a -> (s, b)
execTranslator c startState input = execRWS (runTr c) input startState

translate :: Monoid b => Translator a b s r -> s -> a -> b
translate c startState input = snd $ execTranslator c startState input

updateState :: Monoid b => (s -> s) -> Translator a b s ()
updateState = modify

setState :: Monoid b => s -> Translator a b s ()
setState = updateState . const

fixedPoint :: (Eq a, Monoid a) => Translator a a s r -> Translator a a s r
fixedPoint tr = do
  input <- getNodes
  startState <- getState
  let (newState, output) = execTranslator tr startState input
  if input == output
    then tr
    else runTranslator (setState newState >> fixedPoint tr) output
