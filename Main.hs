import System.IO
import System.Environment
import System.Process
import System.FilePath
import Data.Char
import Data.List
import Data.Maybe
import Text.ParserCombinators.ReadP
import Control.Monad
import Debug.Trace

import Translator

data AdjustMode = Data | Ptr deriving (Show, Eq)
data IR = Adjust AdjustMode Int
        | Input Int
        | Output Int
        | Loop [IR]
        | SetData Int
        deriving (Show, Eq)

type IRProgram = [IR]
data ParseState = ParseState { loopCounter :: Int
                             , openLoops :: [Int]}

parse :: String -> IRProgram
parse = fst . last . readP_to_S parser . filter (`elem` "><+-,.[]")
  where parser = many $ foldl1 (<++) [ bfChar '>' (Adjust Ptr 1)
                                     , bfChar '<' (Adjust Ptr (-1))
                                     , bfChar '+' (Adjust Data 1)
                                     , bfChar '-' (Adjust Data (-1))
                                     , bfChar ',' (Input 1)
                                     , bfChar '.' (Output 1)
                                     , parseLoop
                                     ]
          where bfChar c r = r <$ char c
                parseLoop = do
                  char '['
                  ir <- parser
                  char ']'
                  return (Loop ir)

irToAsm :: Translator IRProgram [String] Int ()
irToAsm = do
  n <- getNodes
  unless (null n) $ do
    let (x:xs) = n
    case x of
      (Adjust mode 1) -> emit $ "inc " ++ asmMode mode
      (Adjust mode (-1)) -> emit $ "dec " ++ asmMode mode
      (Adjust mode n) -> if n > 0
        then emit $ "add " ++ asmMode mode ++ ", " ++ show n
        else emit $ "sub " ++ asmMode mode ++ ", " ++ show (abs n)
      (Input n) -> if n == 1
        then emit "call input"
        else do emit $ "mov rbx, " ++ show n
                emit "call input"
      (Output n) -> if n == 1
        then emit "call output"
        else do emit $ "mov rbx, " ++ show n
                emit "call output"
      (SetData n) -> emit $ "mov " ++ asmMode Data ++ ", 0"
      (Loop ir) -> do updateState (+1)
                      loopNumber <- getState
                      let loopName = show loopNumber
                      emit $ "loop" ++ loopName ++ "start:"
                      emit $ "cmp " ++ asmMode Data ++ ", 0"
                      emit $ "je loop" ++ loopName ++ "end"
                      runTranslator irToAsm ir
                      emit $ "cmp " ++ asmMode Data ++ ", 0"
                      emit $ "jne loop" ++ loopName ++ "start"
                      emit $ "loop" ++ loopName ++ "end:"
    unless (null xs) $ runTranslator irToAsm xs
  where asmMode Ptr = "rbp"
        asmMode Data = "byte [rbp]"

irToFullAsm :: Translator IRProgram [String] Int ()
irToFullAsm = do
  emit "section .data"
  emit "bfdata:"
  emit "times 30000 db 0"
  emit "section .text"
  emit "global _start"
  n <- getNodes
  when (usesInput n) $
    procedure "input" $ do
      syscall "0" "0" "rbp" "rbx"
      emit "mov rbx, 1"
  when (usesOutput n) $
    procedure "output" $ do
      syscall "1" "1" "rbp" "rbx"
      emit "mov rbx, 1"
  emit "_start:"
  emit "mov rbp, bfdata"
  emit "mov rbx, 1"
  irToAsm
  emit "mov rdi, 0"
  emit "mov rax, 60"
  emit "syscall"
  where syscall rax rdi rsi rdx = do
          emit $ "mov rax, " ++ rax
          emit $ "mov rdi, " ++ rdi
          emit $ "mov rsi, " ++ rsi
          emit $ "mov rdx, " ++ rdx
          emit "syscall"
        procedure label body = do
          emit $ label ++ ":"
          body
          emit "ret"
        usesInput = any (\b -> case b of
                            Input _ -> True
                            Loop n -> usesInput n
                            _ -> False)
        usesOutput = any (\b -> case b of
                             Output _ -> True
                             Loop n -> usesOutput n
                             _ -> False)

optimize :: Optimizer IRProgram ()
optimize = do
  OptState _ _ round <- getState
  t3 <- tryOpt 3
  case t3 of
    [Input n, Adjust Ptr a, Input m] -> optManyWhen (n == a) [Input (n+m), Adjust Ptr (n+m-1)]
    [Output n, Adjust Ptr 1, Output m] -> optMany [Output (n+m), Adjust Ptr 1]
    [Output n, Adjust Ptr a, Output m] -> optManyWhen (n == a) [Output (n+m), Adjust Ptr (n+m-1)]
    _ -> tryOptFailed
  t2 <- tryOpt 2
  case t2 of
    [Adjust mode1 a, Adjust mode2 b] -> optWhen (mode1 == mode2) $ Adjust mode1 (a+b)
    _ -> tryOptFailed
  t1 <- tryOpt 1
  case t1 of
    [Loop [Adjust Data _]] -> opt $ SetData 0
    [Loop ir] -> opt $ if round == 0
                       then Loop (runOptimizer optimize ir)
                       else Loop ir
    [Adjust _ 0] -> optZero
    [ir] -> opt ir
    _ -> tryOptFailed
  optRest optimize

main :: IO ()
main = do
  [inputFile,"-o",outputFile] <- getArgs
  bfSource <- readFile inputFile
  let irTranslator = translate irToFullAsm 0
      irOptimizer = runOptimizer optimize
      ir = parse bfSource
      ir' = irOptimizer ir
      asmCode = irTranslator ir'
      asmCode' =  unlines $ map prettifyAsm asmCode
      asmFile = replaceExtensions outputFile ".asm"
      objectFile = replaceExtensions outputFile ".o"
  writeFile asmFile asmCode'
  system ("nasm -f elf64 " ++ asmFile ++ " -o " ++ objectFile)
  system ("ld -o " ++ outputFile ++ " " ++ objectFile)
  return ()
  where prettifyAsm str
          | ":" `isSuffixOf` str = str
          | "section" `isPrefixOf` str = str
          | "times" `isPrefixOf` str = str
          | otherwise = "\t" ++ str
